### fsl-base                2302.2      -->  2303.2
- RF: simplify python interpreter detection in createFSLWrapper - use the interpreter specified in the hashbang line
- BF: fixes to fsl.csh
### fsl-installer           3.3.0       -->  3.4.1
- BF: Make sure temp work dir is deleted as root if necessary
- MNT: Set conda pkgs dir to avoid conflict/collision with user pkgs dirs
- MNT: Copy log file to homedir to make it easier to access
### fsl-misc_tcl            2206.0      -->  2303.0
- RF: Adjust help buttons to go to online docs, as offline docs are no longer shipped with FSL