# FSL wiki migration docathlon (18th-19th October 2023)


![FSL docathlon](docathlon.png)


The venerable [FSL Wiki](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/) is not long for this world, and needs to be put out to pasture. See the bottom of this page for the motivation and reasoning behind this decision.


## Objective: Convert FSL wiki content to markdown


The primary aim of the docathlon is to make a start on converting / updating existing content, and to make as much progress as we can within a two day period. We will follow this up with unscheduled/ad-hoc updates, and perhaps another scheduled sprint if needed - there is no fixed date for the migration over to the new system.

During this docathlon, we are **not** aiming to:
 - *Replicate the existing wiki* - this is an opportunity to clean up the documentation, update old material and remove obsolete content.
 - *Replicate the existing documentation structure* - some aspects of the existing wiki structure are unnecessary and unintuitive, e.g. having separate overview/usage pages where a single page would suffice. Furthermore, we now have the opportunity to organise the content in a more intuitive/hierarchical manner, e.g. grouping related pages together.
 - *Add new content* - let's get all of the existing content sorted first!

We have already done some initial work on the migration - the FSL installation pages, pages on internal development processes used by the FSL team, and a few other bits and pieces have already been migrated.  A prototype of the new system is live at:

https://open.win.ox.ac.uk/pages/fsl/docdev/

And the source files are hosted at:

https://git.fmrib.ox.ac.uk/fsl/docdev/

> Note that these URLs will almost certainly change in the future.


## Logistics


Participation can be in person, remote, or a combination of both. We have booked the Grey Room (ground floor of the WIN@FMRIB Annexe building) from 9-5 on both days. MS Teams links for each day are:

 - [Wednesday 18th October](https://teams.microsoft.com/l/meetup-join/19%3ameeting_OWEyMTkwMWYtYmUxZC00NzQzLTk5ODAtN2NmNzUzMzQ4YzRl%40thread.v2/0?context=%7b%22Tid%22%3a%22cc95de1b-97f5-4f93-b4ba-fe68b852cf91%22%2c%22Oid%22%3a%22c1a2aad6-4ed1-47e0-a4b6-0017c4f6afc5%22%7d)
 - [Thursday 19th October](https://teams.microsoft.com/l/meetup-join/19%3ameeting_YWM0MGI0YTktYmUwYi00MzcxLWIwN2QtNGVmOTdjMWQ4N2Rm%40thread.v2/0?context=%7b%22Tid%22%3a%22cc95de1b-97f5-4f93-b4ba-fe68b852cf91%22%2c%22Oid%22%3a%22c1a2aad6-4ed1-47e0-a4b6-0017c4f6afc5%22%7d)


### Schedule


| **When**                 | **What**                         | **Where**            |
|--------------------------|----------------------------------|----------------------|
| *Wednesday 18th October* |                                  |                      |
| **10am-11am**            | Orientation/planning             | Grey Room / MS Teams |
| **11am-5pm**             | Self-paced documentation hacking | Grey Room / MS Teams |
| **5pm**                  | Pub                              | pub                  |
| *Thursday 19th October*  |                                  |                      |
| **10am-5pm**             | Self-paced documentation hacking | Grey Room / MS Teams |
| **5pm**                  | Pub                              | pub                  |


### How to contribute

 1. Download a local copy of the new documentation site:

        git clone git@git.fmrib.ox.ac.uk:fsl/docdev.git
        cd docdev

 2. Download any recent changes, and then create a branch for your new changes - give your branch a sensible name, e.g.:

        git fetch --all
        git checkout -b add-fnirt-docs origin/main

 3. Add your content (more details on this below)

 4. Add/commit your changes

        git add docs/registration/fnirt.md
        git commit -m "Added FNIRT page"

 5. Push your changes to gitlab

        git push origin add-fnirt-docs

 6. Open a merge request at https://git.fmrib.ox.ac.uk/fsl/docdev/

 7. Repeat steps 2-6 a few times, then go to the pub.


The task of migrating a page over is a little tedious, but is fairly straightforward, and generally involves:

 - Copying text content and media files from the existing wiki into a markdown file (with media files stored alongside the markdown file)
 - Don't forget to add binary data ( images etc ) via LFS!
 - Fixing hyper links and formatting
 - Adding the new content to the new documentation repository on GitLab (via a merge request)

> I did attemnpt to automatically convert the existing content to Markdown, but found the conversion process to be less than ideal, and so far have found it easier to create the new Markdown versions of each page from scratch.


You can preview your changes locally by running a local web server:

    python -m http.server 8000

and then opening http://localhost:8000/docs/ in a web browser.

> If you get an error such as `python: command not found`, try `python3` or `fslpython` instead.


## Organisation of new documentation

In contrast to the unstructured-by-design layout of the old wiki, we now have the opportunity to organise our documentation in a more intuitive/hierarchical manner.

We are proposing the following top-level sections under which all other topics will be organised. Each section can have its own landing page, and it is possible to have additional levels of organisation:

 - *Installation and support* - Installation, configuration, conda, containers, etc
 - *Task fMRI* - FEAT, FLOBS, PNM, etc
 - *Resting state fMRI* - MELODIC, Dual regression, FSLNets, etc
 - *Structural MRI* - FAST, FIRST, etc
 - *Diffusion MRI* - eddy, topup, ptx2, etc
 - *Registration* - FLIRT, FNIRT, etc
 - *Statistics* - randomise, GLM, etc
 - *Utilities* - fslswapdim, FSLeyes, etc
 - *Other topics* - Atlases, orientation, file formats, etc
 - *FSL development* - Release history, compilation instructions, etc


## Sections to be migrated


This is a list of all FSL wiki sections that need to be migrated, along with a suggested file name/location in the new documentation repository.

A GitLab issue has been created for each section - please assign yourself to whatever section(s) you would like to work on - you can this on the GitLab issue page via the _Assigness_ section in the right-hand menu - click on the _Assign yourself_ link. Once you have done this, everybody else will be able to see that you are working on the section in question.

Each GitLab issue page contains links to the old wiki pages, and also links to any attachment files that should also be migrated.


![Assign an issue to yourself on GitLab](assign.png){height=200px}



| **Wiki page**                                                                 | **Suggested location**                   | **GitLab issue**                                        |
|-------------------------------------------------------------------------------|------------------------------------------|---------------------------------------------------------|
| *Registration*                                                                |                                          |                                                         |
| [FUGUE](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FUGUE)                         | `docs/registration/fugue.md`             | [8](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/8)   |
| [FLIRT](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FLIRT)                         | `docs/registration/flirt.md`             | [9](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/9)   |
| [MCFLIRT](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/MCFLIRT)                     | `docs/registration/mcflirt.md`           | [11](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/11) |
| [FNIRT](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FNIRT)                         | `docs/registration/fnirt.md`             | [12](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/12) |
| *Task fMRI*                                                                   |                                          |                                                         |
| [FEAT](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FEAT)                           | `docs/task_fmri/feat.md`                 | [13](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/13) |
| [FLOBS](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FLOBS)                         | `docs/task_fmri/flobs.md`                | [14](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/14) |
| [FSLMotionOutliers](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FSLMotionOutliers) | `docs/task_fmri/fsl_motion_outliers.md`  | [15](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/15) |
| [PNM](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/PNM)                             | `docs/task_fmri/pnm.md`                  | [16](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/16) |
| [PPI](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/PPI)                             | `docs/task_fmri/ppi.md`                  | [17](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/17) |
| *Resting state fMRI*                                                          |                                          |                                                         |
| [DualRegression](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/DualRegression)       | `docs/resting_state/dualregression.md`   | [22](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/22) |
| [ICA\_PNM](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/ICA_PNM)                    | ?                                        | [23](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/23) |
| [MELODIC](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/MELODIC)                     | `docs/resting_state/melodic.md`          | [24](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/24) |
| *Structural MRI*                                                              |                                          |                                                         |
| [BET](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/BET)                             | `docs/structural/bet.md`                 | [25](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/25) |
| [BIANCA](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/BIANCA)                       | `docs/structural/bianca.md`              | [26](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/26) |
| [FAST](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FAST)                           | `docs/structural/fast.md`                | [27](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/27) |
| [FIRST](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FIRST)                         | `docs/structural/first.md`               | [28](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/28) |
| [fsl\_anat](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/fsl_anat)                  | `docs/structural/fsl_anat.md`            | [29](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/29) |
| [FSLVBM](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FSLVBM)                       | `docs/structural/fslvbm.md`              | [30](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/30) |
| [lesion\_filling](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/lesion_filling)      | `docs/structural/lesion_filling.md`      | [31](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/31) |
| [SIENA](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/SIENA)                         | `docs/structural/siena.md`               | [32](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/32) |
| [verbena](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/verbena)                     | `docs/structural/verbena.md`             | [33](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/33) |
| *Diffusion MRI*                                                               |                                          |                                                         |
| [AutoPtx](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/AutoPtx)                     | `docs/diffusion/autoptx.md`              | [34](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/34) |
| [eddy](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/eddy)                           | `docs/diffusion/eddy.md`                 | [35](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/35) |
| [eddyqc](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/eddyqc)                       | `docs/diffusion/eddyqc.md`               | [36](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/36) |
| [FDT](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FDT)                             | `docs/diffusion/index.md`                | [37](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/37) |
| [TBSS](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/TBSS)                           | `docs/diffusion/tbss.md`                 | [38](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/38) |
| [topup](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/topup)                         | `docs/diffusion/topup.md`                | [39](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/39) |
| [XTRACT](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/XTRACT)                       | `docs/diffusion/xtract.md`               | [40](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/40) |
| *Statistics*                                                                  |                                          |                                                         |
| [Cluster](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Cluster)                     | `docs/statistics/cluster.md`             | [41](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/41) |
| [FDR](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FDR)                             | `docs/statistics/fdr.md`                 | [42](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/42) |
| [GLM](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/GLM)                             | `docs/statistics/glm.md`                 | [43](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/43) |
| [Mm](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Mm)                               | `docs/statistics/mm.md`                  | [44](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/44) |
| [Randomise](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Randomise)                 | `docs/statistics/randomise.md`           | [45](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/45) |
| [Swe](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Swe)                             | `docs/statistics/swe.md`                 | [46](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/46) |
| *Other topics*                                                                |                                          |                                                         |
| [Contributors](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Contributors)           | `docs/development/contributors.md`       | [47](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/47) |
| [FSL-MRS](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FSL-MRS)                     | `docs/other/fsl_mrs.md`                  | [48](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/48) |
| [OtherSoftware](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/OtherSoftware)         | `docs/other/other_software.md`           | [49](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/49) |
| [SUSAN](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/SUSAN)                         | `docs/other/susan.md`                    | [50](https://git.fmrib.ox.ac.uk/fsl/docdev/-/issues/50) |


### Sections to be removed

These sections are arguably obsolete, and probably not worth migrating:

 - [FEEDS](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FEEDS)
 - [Formats](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Formats)
 - [FreeSurfer](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FreeSurfer)
 - [FSL FAQ](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FSL%20FAQ)
 - [FslOverview](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslOverview)
 - [FslSge](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslSge)
 - [FslTools](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslTools)
 - [FslView](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslView)
 - [Glossary](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Glossary)
 - [InitialProcessing](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/InitialProcessing)
 - [Melview](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Melview)
 - [Miscvis](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Miscvis)
 - [Nets](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Nets)
 - [SGE Submission FAQ](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/SGE%20submission%20FAQ)


### Sections to be decided upon

These sections may or may not need to be migrated for a range of reasons - several of these sections have documentation hosted independently of the FSL wiki (e.g. documentation for the ASL projects is available at https://asl-docs.readthedocs.io/en/latest/)

 - [asl\_file](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/asl_file)
 - [BASIL](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/BASIL)
 - [baycest](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/baycest)
 - [FABBER](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FABBER)
 - [FIX](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FIX)
 - [FLICA](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FLICA)
 - [FSLNets](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FSLNets)
 - [MIST](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/MIST)
 - [MSM](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/MSM)
 - [oxford\_asl](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/oxford_asl)
 - [PALM](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/PALM)
 - [POSSUM](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/POSSUM)
 - [TIRL](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/TIRL)


## Motivation for the migration

There are a few reasons behind the decision to retire the existing FSL Wiki:

 - [MoinMoin](https://moinmo.in/) (the framework that runs the FSL Wiki) requires Python 2.7, which is no longer supported, and therefore poses a potential security problem from a maintenance point of view. There is a new [Python 3 version of MoinMoin](https://github.com/moinwiki/moin) under development, but it has been under development for a very long time, and there is no schedule for its release, nor any information on how difficult it would be to migrate an existing MoinMoin installation over to the new version when it does become available.

 - At the risk of over-generalising, most people seem to be more comfortable working with Markdwon than with Wiki syntax - the syntax used in the current wiki is specific to MoinMoin and can be quite awkward to work with in some situations, whereas Markdown is widely used across many platforms, and so is more familiar to many people.

 - By migrating our docs to Markdown, we will be able to decouple the documentation content from its presentation. The syntax used in MoinMoin is only used by MoinMoin, so our documentation can currently only be hosted with MoinMoin. In contrast, there are many documentation frameworks out there which support Markdown, and so by migrating our content to Markdown, we are future-proofing ourselves if/when we decide to change frameworks again. In the first instance we are planning to use a framework called docsify (https://docsify.js.org/).

 - Migrating away from MoinMoin will give us more control over theming, and will also allow us to use some more sophisticated features, such as embedding interactive example data sets using the [niivue](https://github.com/niivue/niivue) library.
