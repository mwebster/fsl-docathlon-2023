#!/usr/bin/env python


from   collections import namedtuple
import glob
import os
import os.path as op
import sys
import textwrap as tw

from fsl_ci import gitlab

S        = namedtuple('Section', ('group', 'name', 'suggested_file'))
sections = [
    S('Registration',       'FUGUE',             'fugue.md'),
    S('Registration',       'FLIRT',             'flirt.md'),
    S('Registration',       'MCFLIRT',           'mcflirt.md'),
    S('Registration',       'FNIRT',             'fnirt.md'),
    S('Task fMRI',          'FEAT',              'feat.md'),
    S('Task fMRI',          'FLOBS',             'flobs.md'),
    S('Task fMRI',          'FSLMotionOutliers', 'fsl_motion_outliers.md'),
    S('Task fMRI',          'PNM',               'pnm.md'),
    S('Task fMRI',          'PPI',               'ppi.md'),
    S('Resting state fMRI', 'DualRegression',    'dualregression.md'),
    S('Resting state fMRI', 'ICA_PNM',           '?'),
    S('Resting state fMRI', 'MELODIC',           'melodic.md'),
    S('Structural MRI',     'BET',               'bet.md'),
    S('Structural MRI',     'BIANCA',            'bianca.md'),
    S('Structural MRI',     'FAST',              'fast.md'),
    S('Structural MRI',     'FIRST',             'first.md'),
    S('Structural MRI',     'fsl_anat',          'fsl_anat.md'),
    S('Structural MRI',     'FSLVBM',            'fslvbm.md'),
    S('Structural MRI',     'lesion_filling',    'lesion_filling.md'),
    S('Structural MRI',     'SIENA',             'siena.md'),
    S('Structural MRI',     'verbena',           'verbena.md'),
    S('Diffusion MRI',      'AutoPtx',           'autoptx.md'),
    S('Diffusion MRI',      'eddy',              'eddy.md'),
    S('Diffusion MRI',      'eddyqc',            'eddyqc.md'),
    S('Diffusion MRI',      'FDT',               'index.md'),
    S('Diffusion MRI',      'TBSS',              'tbss.md'),
    S('Diffusion MRI',      'topup',             'topup.md'),
    S('Diffusion MRI',      'XTRACT',            'xtract.md'),
    S('Statistics',         'Cluster',           'cluster.md'),
    S('Statistics',         'FDR',               'fdr.md'),
    S('Statistics',         'GLM',               'glm.md'),
    S('Statistics',         'Mm',                'mm.md'),
    S('Statistics',         'Randomise',         'randomise.md'),
    S('Statistics',         'Swe',               'swe.md'),
    S('Development',        'Contributors',      'contributors.md'),
    S('Other topics',       'FSL-MRS',           'fsl_mrs.md'),
    S('Other topics',       'OtherSoftware',     'other_software.md'),
    S('Other topics',       'SUSAN',             'susan.md'),
]


groupdirs = {
    'Registration'       : 'registration',
    'Task fMRI'          : 'task_fmri',
    'Resting state fMRI' : 'resting_state',
    'Structural MRI'     : 'structural',
    'Diffusion MRI'      : 'diffusion',
    'Statistics'         : 'statistics',
    'Development'        : 'development',
    'Other topics'       : 'other',
}


oldwikidump = sys.argv[1]
server      = 'https://git.fmrib.ox.ac.uk'
oldwikiurl  = 'https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/'
attachurl   = 'https://git.fmrib.ox.ac.uk/paulmc/fsl-docathon-2023/-/blob/main/fsl-wiki-dump-20230817/attachments/'
token       = os.environ['FSL_CI_API_TOKEN']


for s in sections:

    oldurl      = f'{oldwikiurl}{s.name}'
    groupdir    = groupdirs[s.group]
    destfile    = f'docs/{groupdir}/{s.suggested_file}'
    subpages    = list(glob.glob(op.join(oldwikidump, f'{s.name}*.html')))
    attachments = list(glob.glob(op.join(oldwikidump, 'attachments', f'{s.name}*')))

    title       = f'Migrate {s.name} section from FSL wiki'
    desc        = tw.dedent(f"""
    Migrate the {s.name} section from the FSL wiki: {oldurl}

    The suggested group for this section is **{s.group}**:
     - The section content should be migrated into `{destfile}`.
     - An entry for the new page should be added to `docs/{groupdir}/_sidebar.md`.
    """)

    if len(subpages) > 0:
        desc += tw.dedent("""
        This section has the following subpages on the old wiki:
        """)
        for p in subpages:
            p     = op.splitext(op.basename(p))[0]
            p     = p.replace('(2f)', '/')   \
                     .replace('(2d)', '-')   \
                     .replace('(2e)', '.')   \
                     .replace('(20)', '%20')
            desc += f' - [{p}]({oldwikiurl}{p})\n'

    if len(attachments) > 0:
        desc += tw.dedent("""
        Attachments for this section can be found at:
        """)
        for att in attachments:
            att   = op.basename(att)
            desc += f' - {attachurl}{att}\n'

    desc = desc.strip()
    gitlab.open_issue('fsl/docdev', title, desc, server, token)
